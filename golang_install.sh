#!/bin/bash

## Copyright (c) 2018 Alan Dinneen alan@implauzable.com ##

if [ $# -eq 0 ]
    then 
        GOVERSION="1.10"
    else
        GOVERSION=$1
fi

install_go() {
    curl https://dl.google.com/go/go$GOVERSION.linux-amd64.tar.gz --output golang.tar.gz
    tar -C /usr/local -xzf golang.tar.gz
    export PATH=$PATH:/usr/local/go/bin
}


add_to_profile() {
   export PATH=$PATH:/user/local/go/bin
   echo '#### GOlang Path' >> $HOME/.bashrc
   echo 'PATH=$PATH:/usr/local/go/bin' >> $HOME/.bashrc
}


remove()) {
    rm -rf /usr/local/go
}


remove_reinstall(){
    remove
    install_go
}

echo "Installing golang version $GOVERSION"
install_go
#add_to_profile
